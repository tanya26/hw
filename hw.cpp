#include <iostream>
#include <string>
#include <cassert>
#include <sstream>
#include "collector.h"

const int MAX_NAME = 100;
const int MIN_PRICE = 0;
const int MAX_PRICE = 200000;
const int MIN_CAL = 0;
const int MAX_CAL = 5000;
const int MIN_TIME = 0;
const int MAX_TIME = 120;
const int MIN_POPULAR= 0;
const int MAX_POPULAR= 5;

class FastFoodRestaurant : public ICollectable {
 protected:
  bool invariant() const {
	return _name.size() <= MAX_NAME && _price >= MIN_PRICE && _price <= MAX_PRICE && _calories >= MIN_CAL && _calories <= MAX_CAL
	&& _time >= MIN_TIME && _time <= MAX_TIME && _popular >= MIN_POPULAR && _popular <= MAX_POPULAR;
  }

 public:
  FastFoodRestaurant() = delete;

  FastFoodRestaurant(const FastFoodRestaurant& p) = delete;

  FastFoodRestaurant& operator=(const FastFoodRestaurant& p) = delete;

  FastFoodRestaurant(std::string name, int price, int calories, int time, int popular): _name(std::move(name)), _price(price), _calories(calories), _time(time), _popular(popular)  {
	assert(invariant());
  }

  const std::string& getName() const {
    return _name;
  }

  int getPrice() const {
    return _price;
  }

  int getCalories() const {
    return _calories;
  }

  int getTime() const {
	return _time;
  }

  int getPopular() const {
	return _popular;
  }

  bool write(std::ostream& os) override {
	writeString(os, _name);
	writeNumber(os, _price);
	writeNumber(os, _calories);
	writeNumber(os, _time);
	writeNumber(os, _popular);
	return os.good();
  }

 private:
  std::string _name;
  int _price;
  int _calories;
  int _time;
  int _popular;
};

class ItemCollector: public ACollector {
 public:
  std::shared_ptr<ICollectable> read(std::istream& is) override {
	std::string name = readString(is, MAX_NAME);
	int price = readNumber<int>(is);
	int calories = readNumber<int>(is);
	int time = readNumber<int>(is);
	int popular = readNumber<int>(is);
	return std::make_shared<FastFoodRestaurant>(name, price, calories, time, popular);
  }
};

bool performCommand(const std::vector<std::string> & args, ItemCollector & col) {
  if (args.empty()) {
	return false;
  }

  if (args[0] == "l" || args[0] == "load") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!col.loadCollection(filename)) {
	  std::cerr << "Ошибка при загрузке файла '" << filename << "'" << std::endl;
	  return false;
	}
	return true;
  }

  if (args[0] == "s" || args[0] == "save") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!col.saveCollection(filename)) {
	  std::cerr << "Ошибка при сохранении файла '" << filename << "'" << std::endl;
	  return false;
	}
	return true;
  }

  if (args[0] == "c" || args[0] == "clean") {
	if (args.size() != 1) {
	  std::cerr << "Некорректное количество аргументов команды clean" << std::endl;
	  return false;
	}
	col.clean();
	return true;
  }

  if (args[0] == "a" || args[0] == "add") {
	if (args.size() != 6) {
	  std::cerr << "Некорректное количество аргументов команды add" << std::endl;
	  return false;
	}

	col.addItem(std::make_shared<FastFoodRestaurant>(args[1], std::stoi(args[2]), std::stoi(args[3]), std::stoi(args[4]), std::stoi(args[5])));
	return true;
  }

  if (args[0] == "r" || args[0] == "remove") {
	if (args.size() != 2) {
	  std::cerr << "Некорректное количество аргументов команды remove" << std::endl;
	  return false;
	}
	col.removeItem(std::stoi(args[1]));
	return true;
  }

  if (args[0] == "u" || args[0] == "update") {
	if (args.size() != 7) {
	  std::cerr << "Некорректное количество аргументов команды update" << std::endl;
	  return false;
	}
	col.updateItem(stoul(args[1]), std::make_shared<FastFoodRestaurant>(args[2], std::stoi(args[3]), std::stoi(args[4]), std::stoi(args[5]), std::stoi(args[6])));
	return true;
  }

  if (args[0] == "v" || args[0] == "view") {
	if (args.size() != 1) {
	  std::cerr << "Некорректное количество аргументов команды view" << std::endl;
	  return false;
	}

	size_t count = 0;
	for(size_t i = 0; i < col.getSize(); ++i) {
	  const FastFoodRestaurant & item = dynamic_cast<FastFoodRestaurant &>(*col.getItem(i));
	  if (!col.isRemoved(i)) {
		std::cout << "[" << i << "] "
		<< "Наименование блюда: " << item.getName()
		<< " Стоимость: " << item.getPrice()
		<< " Количество калорий: " << item.getCalories()
		<< " Время приготовлени(мин.): " << item.getTime()
		<< " Популярность: " << item.getPopular() << std::endl;
		count ++;
	  }
	}

	std::cout << "Количество элементов в коллекции: " << count << std::endl;
	return true;
  }

  std::cerr << "Недопустимая команда '" << args[0] << "'" << std::endl;
  return false;
}

int main(int , char **) {
  ItemCollector col;

  for (std::string line; getline(std::cin, line); ) {
	if (line.empty()) {
	  break;
	}

	std::istringstream  iss(line);
	std::vector<std::string> args;

	for(std::string str; iss.good();) {
	  iss >> str;
	  args.emplace_back(str);
	}

	if (!performCommand(args, col)) {
	  return 1;
	}
  }

  std::cout << "Выполнение завершено успешно" << std::endl;
  return 0;
}
